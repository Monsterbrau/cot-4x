import { Map } from "../board/map/map";
import { Colon } from "../board/player/civil/colon/colon";
import { Army } from "../board/player/army/army";
import { Player } from "../board/player/player";
import { selectUnit } from "../game/move";

export function start() {
  //pre test config

  //nbPlayer < 5
  let nbPlayer = 2;
  //

  let mapSize = 15;
  //

  let initialMap = _initializeMap(mapSize, nbPlayer);
  let players = _initializePlayer(nbPlayer);

  _InitialStartPlayer(players, initialMap );
  selectUnit();
  
}

function _initializeMap(nb, players) {
  let map = new Map(nb, players);
  return map.generate();
}

function _initializePlayer(nbPlayer) {
  let players = [];
  if (nbPlayer < 5) {
    for (let i = 1; i <= nbPlayer; i++) {
      players.push(new Player(`joueur ${i}`));
    }
  }
  return players;
}

function _starterPlayer(square, player) {
  let firstArmy = new Army(player.pseudo);
  let firstColon = new Colon(player.pseudo);
  firstArmy.colon = firstColon;
  firstArmy.number = 1;
  square.units = firstArmy;
  document
    .getElementsByClassName(`${square.x}-${square.y}`)[0]
    .classList.add(firstArmy.class);
}

function _InitialStartPlayer(gamePlayers, map) {
  let end = map.length - 1;
  let initial = [
    { x: 1, y: 1 },
    { x: 1, y: end },
    { x: end-1, y: 1 },
    { x: end-1, y: end }
  ];

  // check starter biome and change square if biome : see
  // and initialize each player

  for (let i = 0; i < gamePlayers.length; i++) {
    switch (i) {
      case 0:
        for (let index = 0; index < map[initial[0].x].length; index++) {
          if (map[initial[i].x][index].biome !== "see") {
            _starterPlayer(map[initial[i].x][index], gamePlayers[i]);
            index = map[initial[0].x].length;
          }
        }
        break;
      case 1:
        for (let index = map[initial[i].x].length; index > 0; index--) {
          if (map[initial[i].x][index - 1].biome !== "see") {
            _starterPlayer(map[initial[i].x][index - 1], gamePlayers[i]);
            index = 0;
          }
        }
        break;
      case 2:
        for (let index = 0; index < map[initial[i].x].length; index++) {
          if (map[initial[i].x][index].biome !== "see") {
            _starterPlayer(map[initial[i].x][index], gamePlayers[i]);
            index = map[initial[i].x].length;
          }
        }
        break;
      default:
        for (let index = map[initial[i].x].length; index > 0; index--) {        
          if (map[initial[i].x][index - 1].biome !== "see") {
            _starterPlayer(map[initial[i].x][index - 1], gamePlayers[i]);
            index = 0;
          }
        }
        break;
    }
  }
  
  localStorage.setItem('map',JSON.stringify(map));
}
