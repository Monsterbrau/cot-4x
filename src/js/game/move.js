"use strict";

export function selectUnit() {
  let board = document.getElementById("board");
  board.addEventListener("click", _move);
}

export function selectedTdFront(x, y) {
  return document.getElementsByClassName(`${x}-${y}`)[0];
}

export function selectedClassFront(name) {
  return document.getElementsByClassName(name)[0];
}

function _move(event) {
  if (event.target.classList.contains("army")) {
    document.getElementById("armyActions").innerHTML = "";

    //check if other selection and re-initalize
    if (document.getElementsByClassName("selected").length !== 0) {
      for (let selected of document.getElementsByClassName("selected")) {
        let x = selected.classList[0].split("-")[0];
        let y = selected.classList[0].split("-")[1];
        selectedTdFront(x, y).classList.remove("selected");
      }
    }
    event.target.classList.add("selected");

    //generate front aside : army action when unit get selected
    let x = event.target.classList[0].split("-")[0];
    let y = event.target.classList[0].split("-")[1];
    let actualMap = JSON.parse(localStorage.getItem("map"));
    let aside = document.getElementById("armyActions");

    if (actualMap[x][y].units.number) {
      let armyFront = document.createElement("h3");
      let army = actualMap[x][y].units;
      armyFront.textContent = `Army : ${army.number} - Player : ${army.player}`;
      aside.appendChild(armyFront);

    }
    
    if (actualMap[x][y].city.name) {
      let cityFront = document.createElement("h3");
      let city = actualMap[x][y].city;
      cityFront.textContent = `City : ${city.name} - Player : ${city.realms}`;
      aside.appendChild(cityFront);
    }
    
    //

    document.addEventListener("keypress", _go);
  }
}

function _go(event) {
  let td = document.getElementsByClassName(`selected`)[0];
  let x = td.classList[0].split("-")[0];
  let y = td.classList[0].split("-")[1];

  let actualMap = JSON.parse(localStorage.getItem("map"));

  let line = x;
  let square = y;

  switch (event.charCode) {
    case 55:
      line--;
      square--;
      break;
    case 56:
      line--;
      break;
    case 57:
      line--;
      square++;
      break;
    case 52:
      square--;
      break;
    case 54:
      square++;
      break;
    case 49:
      line++;
      square--;
      break;
    case 50:
      line++;
      break;
    case 51:
      line++, square++;
      break;
    case 98:
      if (actualMap[x][y].units.colon) {
console.log(actualMap[x][y].units.colon);

        actualMap[x][y].units.colon.foundCity(actualMap[x][y], "test");
      }
      break;
    default:
      break;
  }
  _finalMove(line, square, actualMap[x][y], actualMap);
}

function _finalMove(line, square, initialStatSquare, actualMap) {
  //check biome see
  if (actualMap[line][square].biome !== "see") {
    actualMap[line][square].units = initialStatSquare.units;
    
    initialStatSquare.units = {};
    selectedTdFront(initialStatSquare.x, initialStatSquare.y).classList.remove(
      "selected",
      "army"
    );

    selectedTdFront(line, square).classList.add("army");

    //save new map in local storage
    localStorage.setItem("map", JSON.stringify(actualMap));
    document.removeEventListener("keypress", _go);
  }
}
