"use strict";

import { Unit } from "../../unit";
import { selectedTdFront } from "../../../../game/move";
import { City } from "../../../../player/city";

export class Colon extends Unit {
  constructor() {
    super();
    this.name = "colon";
    this.stamina = 1;
  }
  foundCity(square, cityName) {
     let map = JSON.parse(localStorage.getItem("map"));
     let testCity = true;
     // let okCity = [
     //   map[square.x--][square.y--],
     //   map[square.x][square.y--],
     //   map[square.x++][square.y--],
     //   map[square.x--][square.y],
     //   map[square.x++][square.y],
     //   map[square.x--][square.y++],
     //   map[square.x][square.y++],
     //   map[square.x++][square.y++]
     // ];
   
     // for (let item of okCity) {
     //   if (item.city.name) {
     //     testCity = false;
     //     break;
     //   }
     // }
   
     // if (testCity) {
       selectedTdFront(square.x, square.y).classList.add("city");
   
       map[square.x][square.y].city = new City(square.units.player, cityName);
       localStorage.setItem("map", JSON.stringify(map));
     // }
   }
}

