"use strict"

export class Player {
    constructor(pseudo) {
        this.pseudo = pseudo;
        this.civilization = {};
        this.armies = [];
        this.color;
    }
}