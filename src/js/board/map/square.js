import json from "../../json/data";

("use strict");

export class Square {
  constructor(x, y) {
    this.biome;
    this.x = x;
    this.y = y;
    this.ressources = {};
    this.units = [];
    this.city = {};
  }

  _generateBiome() {
    let nbBiome = Math.floor(Math.random() * 100);
    if (nbBiome <= 35) {
      this.biome = "plains";
    } else if (36 <= nbBiome && nbBiome <= 60) {
      this.biome = "forests";
    } else if (61 <= nbBiome && nbBiome <= 70) {
      this.biome = "mountains";
    } else {
      this.biome = "see";
    }
  }

  _generateBasicsRessources() {
    this.ressources.basics = json.ressources[this.biome].basics;
  }

  generating() {
    this._generateBiome();
    this._generateBasicsRessources();
  }
}
