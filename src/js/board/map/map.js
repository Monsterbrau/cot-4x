import { Square } from "./square";
import json from '../../json/data'

("use strict");

export class Map {
  constructor(map, nbPlayers) {
    this.map = map;
    this.nbPlayers = nbPlayers;
  }
  generate() {
    let grid = [];
    let board = document.querySelector("#board");

    for (let x = 0; x < this.map; x++) {
      let line = document.createElement("tr");
      board.appendChild(line);
      grid[x] = [];
      for (let y = 0; y < this.map; y++) {
        let drawRoom = document.createElement("td");
        let square = grid[x][y];
        square = new Square(x, y);
        square.generating();

        //add see around map
        
        if (square.x === 0 || square.y === 0 || square.x === this.map -1 || square.y=== this.map -1) {
          square.biome = 'see';
          square.ressources = json.ressources[square.biome];
        }

        //

        drawRoom.classList.add(`${square.x}-${square.y}`);
        drawRoom.classList.add(square.biome);

        line.appendChild(drawRoom);
        grid[x].push(square);
      }
    }
    
    return grid;
  }
}
