/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _js_game_game__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/game/game */ \"./src/js/game/game.js\");\n\r\n\r\nObject(_js_game_game__WEBPACK_IMPORTED_MODULE_0__[\"start\"])();\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/js/board/map/map.js":
/*!*********************************!*\
  !*** ./src/js/board/map/map.js ***!
  \*********************************/
/*! exports provided: Map */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Map\", function() { return Map; });\n/* harmony import */ var _square__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./square */ \"./src/js/board/map/square.js\");\n/* harmony import */ var _json_data__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../json/data */ \"./src/js/json/data.json\");\nvar _json_data__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../json/data */ \"./src/js/json/data.json\", 1);\n\r\n\r\n\r\n(\"use strict\");\r\n\r\nclass Map {\r\n  constructor(map, nbPlayers) {\r\n    this.map = map;\r\n    this.nbPlayers = nbPlayers;\r\n  }\r\n  generate() {\r\n    let grid = [];\r\n    let board = document.querySelector(\"#board\");\r\n\r\n    for (let x = 0; x < this.map; x++) {\r\n      let line = document.createElement(\"tr\");\r\n      board.appendChild(line);\r\n      grid[x] = [];\r\n      for (let y = 0; y < this.map; y++) {\r\n        let drawRoom = document.createElement(\"td\");\r\n        let square = grid[x][y];\r\n        square = new _square__WEBPACK_IMPORTED_MODULE_0__[\"Square\"](x, y);\r\n        square.generating();\r\n\r\n        //add see around map\r\n        \r\n        if (square.x === 0 || square.y === 0 || square.x === this.map -1 || square.y=== this.map -1) {\r\n          square.biome = 'see';\r\n          square.ressources = _json_data__WEBPACK_IMPORTED_MODULE_1__.ressources[square.biome];\r\n        }\r\n\r\n        //\r\n\r\n        drawRoom.classList.add(`${square.x}-${square.y}`);\r\n        drawRoom.classList.add(square.biome);\r\n\r\n        line.appendChild(drawRoom);\r\n        grid[x].push(square);\r\n      }\r\n    }\r\n    \r\n    return grid;\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/board/map/map.js?");

/***/ }),

/***/ "./src/js/board/map/square.js":
/*!************************************!*\
  !*** ./src/js/board/map/square.js ***!
  \************************************/
/*! exports provided: Square */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Square\", function() { return Square; });\n/* harmony import */ var _json_data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../json/data */ \"./src/js/json/data.json\");\nvar _json_data__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../json/data */ \"./src/js/json/data.json\", 1);\n\r\n\r\n(\"use strict\");\r\n\r\nclass Square {\r\n  constructor(x, y) {\r\n    this.biome;\r\n    this.x = x;\r\n    this.y = y;\r\n    this.ressources = {};\r\n    this.units = [];\r\n    this.city = {};\r\n  }\r\n\r\n  _generateBiome() {\r\n    let nbBiome = Math.floor(Math.random() * 100);\r\n    if (nbBiome <= 35) {\r\n      this.biome = \"plains\";\r\n    } else if (36 <= nbBiome && nbBiome <= 60) {\r\n      this.biome = \"forests\";\r\n    } else if (61 <= nbBiome && nbBiome <= 70) {\r\n      this.biome = \"mountains\";\r\n    } else {\r\n      this.biome = \"see\";\r\n    }\r\n  }\r\n\r\n  _generateBasicsRessources() {\r\n    this.ressources.basics = _json_data__WEBPACK_IMPORTED_MODULE_0__.ressources[this.biome].basics;\r\n  }\r\n\r\n  generating() {\r\n    this._generateBiome();\r\n    this._generateBasicsRessources();\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/board/map/square.js?");

/***/ }),

/***/ "./src/js/board/player/army/army.js":
/*!******************************************!*\
  !*** ./src/js/board/player/army/army.js ***!
  \******************************************/
/*! exports provided: Army */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Army\", function() { return Army; });\n// \"use strict\"\r\n\r\nclass Army {\r\n    constructor(playerPseudo) {\r\n        this.player = playerPseudo;\r\n        this.number;\r\n        this.general ={};\r\n        this.infantery = {};\r\n        this.cavalery = {};\r\n        this.artillery = {};\r\n        this.colon = {};\r\n        this.class = 'army';\r\n\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/js/board/player/army/army.js?");

/***/ }),

/***/ "./src/js/board/player/civil/colon/colon.js":
/*!**************************************************!*\
  !*** ./src/js/board/player/civil/colon/colon.js ***!
  \**************************************************/
/*! exports provided: Colon */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Colon\", function() { return Colon; });\n/* harmony import */ var _unit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../unit */ \"./src/js/board/player/unit.js\");\n/* harmony import */ var _game_move__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../game/move */ \"./src/js/game/move.js\");\n/* harmony import */ var _player_city__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../player/city */ \"./src/js/player/city.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nclass Colon extends _unit__WEBPACK_IMPORTED_MODULE_0__[\"Unit\"] {\r\n  constructor() {\r\n    super();\r\n    this.name = \"colon\";\r\n    this.stamina = 1;\r\n  }\r\n  foundCity(square, cityName) {\r\n     let map = JSON.parse(localStorage.getItem(\"map\"));\r\n     let testCity = true;\r\n     // let okCity = [\r\n     //   map[square.x--][square.y--],\r\n     //   map[square.x][square.y--],\r\n     //   map[square.x++][square.y--],\r\n     //   map[square.x--][square.y],\r\n     //   map[square.x++][square.y],\r\n     //   map[square.x--][square.y++],\r\n     //   map[square.x][square.y++],\r\n     //   map[square.x++][square.y++]\r\n     // ];\r\n   \r\n     // for (let item of okCity) {\r\n     //   if (item.city.name) {\r\n     //     testCity = false;\r\n     //     break;\r\n     //   }\r\n     // }\r\n   \r\n     // if (testCity) {\r\n       Object(_game_move__WEBPACK_IMPORTED_MODULE_1__[\"selectedTdFront\"])(square.x, square.y).classList.add(\"city\");\r\n   \r\n       map[square.x][square.y].city = new _player_city__WEBPACK_IMPORTED_MODULE_2__[\"City\"](square.units.player, cityName);\r\n       localStorage.setItem(\"map\", JSON.stringify(map));\r\n     // }\r\n   }\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/js/board/player/civil/colon/colon.js?");

/***/ }),

/***/ "./src/js/board/player/player.js":
/*!***************************************!*\
  !*** ./src/js/board/player/player.js ***!
  \***************************************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Player\", function() { return Player; });\n\r\n\r\nclass Player {\r\n    constructor(pseudo) {\r\n        this.pseudo = pseudo;\r\n        this.civilization = {};\r\n        this.armies = [];\r\n        this.color;\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/js/board/player/player.js?");

/***/ }),

/***/ "./src/js/board/player/unit.js":
/*!*************************************!*\
  !*** ./src/js/board/player/unit.js ***!
  \*************************************/
/*! exports provided: Unit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Unit\", function() { return Unit; });\n\r\n\r\nclass Unit {\r\n  constructor() {\r\n    this.nbActions = 1;\r\n    this.strenght = 0;\r\n    this.stamina = 0;\r\n    this.armor = 0;\r\n    this.number = 0;\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/board/player/unit.js?");

/***/ }),

/***/ "./src/js/game/game.js":
/*!*****************************!*\
  !*** ./src/js/game/game.js ***!
  \*****************************/
/*! exports provided: start */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"start\", function() { return start; });\n/* harmony import */ var _board_map_map__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../board/map/map */ \"./src/js/board/map/map.js\");\n/* harmony import */ var _board_player_civil_colon_colon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../board/player/civil/colon/colon */ \"./src/js/board/player/civil/colon/colon.js\");\n/* harmony import */ var _board_player_army_army__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../board/player/army/army */ \"./src/js/board/player/army/army.js\");\n/* harmony import */ var _board_player_player__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../board/player/player */ \"./src/js/board/player/player.js\");\n/* harmony import */ var _game_move__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../game/move */ \"./src/js/game/move.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction start() {\r\n  //pre test config\r\n\r\n  //nbPlayer < 5\r\n  let nbPlayer = 2;\r\n  //\r\n\r\n  let mapSize = 15;\r\n  //\r\n\r\n  let initialMap = _initializeMap(mapSize, nbPlayer);\r\n  let players = _initializePlayer(nbPlayer);\r\n\r\n  _InitialStartPlayer(players, initialMap );\r\n  Object(_game_move__WEBPACK_IMPORTED_MODULE_4__[\"selectUnit\"])();\r\n  \r\n}\r\n\r\nfunction _initializeMap(nb, players) {\r\n  let map = new _board_map_map__WEBPACK_IMPORTED_MODULE_0__[\"Map\"](nb, players);\r\n  return map.generate();\r\n}\r\n\r\nfunction _initializePlayer(nbPlayer) {\r\n  let players = [];\r\n  if (nbPlayer < 5) {\r\n    for (let i = 1; i <= nbPlayer; i++) {\r\n      players.push(new _board_player_player__WEBPACK_IMPORTED_MODULE_3__[\"Player\"](`joueur ${i}`));\r\n    }\r\n  }\r\n  return players;\r\n}\r\n\r\nfunction _starterPlayer(square, player) {\r\n  let firstArmy = new _board_player_army_army__WEBPACK_IMPORTED_MODULE_2__[\"Army\"](player.pseudo);\r\n  let firstColon = new _board_player_civil_colon_colon__WEBPACK_IMPORTED_MODULE_1__[\"Colon\"](player.pseudo);\r\n  firstArmy.colon = firstColon;\r\n  firstArmy.number = 1;\r\n  square.units = firstArmy;\r\n  document\r\n    .getElementsByClassName(`${square.x}-${square.y}`)[0]\r\n    .classList.add(firstArmy.class);\r\n}\r\n\r\nfunction _InitialStartPlayer(gamePlayers, map) {\r\n  let end = map.length - 1;\r\n  let initial = [\r\n    { x: 1, y: 1 },\r\n    { x: 1, y: end },\r\n    { x: end-1, y: 1 },\r\n    { x: end-1, y: end }\r\n  ];\r\n\r\n  // check starter biome and change square if biome : see\r\n  // and initialize each player\r\n\r\n  for (let i = 0; i < gamePlayers.length; i++) {\r\n    switch (i) {\r\n      case 0:\r\n        for (let index = 0; index < map[initial[0].x].length; index++) {\r\n          if (map[initial[i].x][index].biome !== \"see\") {\r\n            _starterPlayer(map[initial[i].x][index], gamePlayers[i]);\r\n            index = map[initial[0].x].length;\r\n          }\r\n        }\r\n        break;\r\n      case 1:\r\n        for (let index = map[initial[i].x].length; index > 0; index--) {\r\n          if (map[initial[i].x][index - 1].biome !== \"see\") {\r\n            _starterPlayer(map[initial[i].x][index - 1], gamePlayers[i]);\r\n            index = 0;\r\n          }\r\n        }\r\n        break;\r\n      case 2:\r\n        for (let index = 0; index < map[initial[i].x].length; index++) {\r\n          if (map[initial[i].x][index].biome !== \"see\") {\r\n            _starterPlayer(map[initial[i].x][index], gamePlayers[i]);\r\n            index = map[initial[i].x].length;\r\n          }\r\n        }\r\n        break;\r\n      default:\r\n        for (let index = map[initial[i].x].length; index > 0; index--) {        \r\n          if (map[initial[i].x][index - 1].biome !== \"see\") {\r\n            _starterPlayer(map[initial[i].x][index - 1], gamePlayers[i]);\r\n            index = 0;\r\n          }\r\n        }\r\n        break;\r\n    }\r\n  }\r\n  \r\n  localStorage.setItem('map',JSON.stringify(map));\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/game/game.js?");

/***/ }),

/***/ "./src/js/game/move.js":
/*!*****************************!*\
  !*** ./src/js/game/move.js ***!
  \*****************************/
/*! exports provided: selectUnit, selectedTdFront, selectedClassFront */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"selectUnit\", function() { return selectUnit; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"selectedTdFront\", function() { return selectedTdFront; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"selectedClassFront\", function() { return selectedClassFront; });\n\r\n\r\nfunction selectUnit() {\r\n  let board = document.getElementById(\"board\");\r\n  board.addEventListener(\"click\", _move);\r\n}\r\n\r\nfunction selectedTdFront(x, y) {\r\n  return document.getElementsByClassName(`${x}-${y}`)[0];\r\n}\r\n\r\nfunction selectedClassFront(name) {\r\n  return document.getElementsByClassName(name)[0];\r\n}\r\n\r\nfunction _move(event) {\r\n  if (event.target.classList.contains(\"army\")) {\r\n    document.getElementById(\"armyActions\").innerHTML = \"\";\r\n\r\n    //check if other selection and re-initalize\r\n    if (document.getElementsByClassName(\"selected\").length !== 0) {\r\n      for (let selected of document.getElementsByClassName(\"selected\")) {\r\n        let x = selected.classList[0].split(\"-\")[0];\r\n        let y = selected.classList[0].split(\"-\")[1];\r\n        selectedTdFront(x, y).classList.remove(\"selected\");\r\n      }\r\n    }\r\n    event.target.classList.add(\"selected\");\r\n\r\n    //generate front aside : army action when unit get selected\r\n    let x = event.target.classList[0].split(\"-\")[0];\r\n    let y = event.target.classList[0].split(\"-\")[1];\r\n    let actualMap = JSON.parse(localStorage.getItem(\"map\"));\r\n    let aside = document.getElementById(\"armyActions\");\r\n\r\n    if (actualMap[x][y].units.number) {\r\n      let armyFront = document.createElement(\"h3\");\r\n      let army = actualMap[x][y].units;\r\n      armyFront.textContent = `Army : ${army.number} - Player : ${army.player}`;\r\n      aside.appendChild(armyFront);\r\n\r\n    }\r\n    \r\n    if (actualMap[x][y].city.name) {\r\n      let cityFront = document.createElement(\"h3\");\r\n      let city = actualMap[x][y].city;\r\n      cityFront.textContent = `City : ${city.name} - Player : ${city.realms}`;\r\n      aside.appendChild(cityFront);\r\n    }\r\n    \r\n    //\r\n\r\n    document.addEventListener(\"keypress\", _go);\r\n  }\r\n}\r\n\r\nfunction _go(event) {\r\n  let td = document.getElementsByClassName(`selected`)[0];\r\n  let x = td.classList[0].split(\"-\")[0];\r\n  let y = td.classList[0].split(\"-\")[1];\r\n\r\n  let actualMap = JSON.parse(localStorage.getItem(\"map\"));\r\n\r\n  let line = x;\r\n  let square = y;\r\n\r\n  switch (event.charCode) {\r\n    case 55:\r\n      line--;\r\n      square--;\r\n      break;\r\n    case 56:\r\n      line--;\r\n      break;\r\n    case 57:\r\n      line--;\r\n      square++;\r\n      break;\r\n    case 52:\r\n      square--;\r\n      break;\r\n    case 54:\r\n      square++;\r\n      break;\r\n    case 49:\r\n      line++;\r\n      square--;\r\n      break;\r\n    case 50:\r\n      line++;\r\n      break;\r\n    case 51:\r\n      line++, square++;\r\n      break;\r\n    case 98:\r\n      if (actualMap[x][y].units.colon) {\r\nconsole.log(actualMap[x][y].units.colon);\r\n\r\n        actualMap[x][y].units.colon.foundCity(actualMap[x][y], \"test\");\r\n      }\r\n      break;\r\n    default:\r\n      break;\r\n  }\r\n  _finalMove(line, square, actualMap[x][y], actualMap);\r\n}\r\n\r\nfunction _finalMove(line, square, initialStatSquare, actualMap) {\r\n  //check biome see\r\n  if (actualMap[line][square].biome !== \"see\") {\r\n    actualMap[line][square].units = initialStatSquare.units;\r\n    \r\n    initialStatSquare.units = {};\r\n    selectedTdFront(initialStatSquare.x, initialStatSquare.y).classList.remove(\r\n      \"selected\",\r\n      \"army\"\r\n    );\r\n\r\n    selectedTdFront(line, square).classList.add(\"army\");\r\n\r\n    //save new map in local storage\r\n    localStorage.setItem(\"map\", JSON.stringify(actualMap));\r\n    document.removeEventListener(\"keypress\", _go);\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/game/move.js?");

/***/ }),

/***/ "./src/js/json/data.json":
/*!*******************************!*\
  !*** ./src/js/json/data.json ***!
  \*******************************/
/*! exports provided: biome, ressources, default */
/***/ (function(module) {

eval("module.exports = JSON.parse(\"{\\\"biome\\\":[\\\"mountains\\\",\\\"plains\\\",\\\"see\\\",\\\"forests\\\"],\\\"ressources\\\":{\\\"mountains\\\":{\\\"basics\\\":{\\\"food\\\":0,\\\"wood\\\":0,\\\"stone\\\":4}},\\\"see\\\":{\\\"basics\\\":{\\\"food\\\":4,\\\"wood\\\":0,\\\"stone\\\":0}},\\\"plains\\\":{\\\"basics\\\":{\\\"food\\\":3,\\\"wood\\\":1,\\\"stone\\\":0}},\\\"forests\\\":{\\\"basics\\\":{\\\"food\\\":1,\\\"wood\\\":3,\\\"stone\\\":0}}}}\");\n\n//# sourceURL=webpack:///./src/js/json/data.json?");

/***/ }),

/***/ "./src/js/player/city.js":
/*!*******************************!*\
  !*** ./src/js/player/city.js ***!
  \*******************************/
/*! exports provided: City */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"City\", function() { return City; });\n\r\n\r\nclass City {\r\n    constructor(playerName, cityName){\r\n        this.name= cityName;\r\n        this.realms = playerName;\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/js/player/city.js?");

/***/ })

/******/ });